<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
       .message{
            color: pink;
            text-align: center;
       }
    </style>
</head>
<body>
    <?php
        $listResult = array("1", "HELLO WORLD!", "Tất cả các ý trên","null", "$", "define","0.5","===","PHP: Hypertext Preprocessor","ab");
        $selectionList = array();
        $score = 0;
        for($i = 1 ; $i<=10 ; $i++){
            $key = "answer".strval($i);
            if($_COOKIE[$key] == $listResult[$i-1]) 
                $score++;
        }

        echo "<h2 class='message'>Bạn đạt được : $score/10 </h2>";
        if($score<4)
            echo "<div class='message'>Bạn quá kém, cần ôn tập thêm</div>";
        elseif($score<=7)
            echo "<div class='message'>Cũng bình thường</div>";
        else
            echo "<div class='message'>Sắp sửa làm được trợ giảng lớp PHP</div>";
    ?>
</body>
</html>
