<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        session_start();
    
        $listQA1 = array("1","2","3","Không có đáp án");
        $listQA2 = array("HELLO WORLD!","hello world!","hello wolrd","Không chạy được, báo lỗi");
        $listQA3 = array("/*commented code here */","// you are handsome","# you are beauty","Tất cả các ý trên");
        $listQA4 = array("none","null","undef","Không có khái niệm như vậy trong PHP");
        $listQA5 = array("$$","$","@","#");

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            
            if(isset($_POST["answer1"])&&isset($_POST["answer2"])&&isset($_POST["answer3"])&&isset($_POST["answer4"])&&isset($_POST["answer5"])){
                $answer1 = $_POST["answer1"];
                $answer2 = $_POST["answer2"];
                $answer3 = $_POST["answer3"];
                $answer4 = $_POST["answer4"];
                $answer5 = $_POST["answer5"];
                setcookie("answer1",$answer1, time() + (86400 * 30), "/");
                setcookie("answer2",$answer2, time() + (86400 * 30), "/");
                setcookie("answer3",$answer3, time() + (86400 * 30), "/");
                setcookie("answer4",$answer4, time() + (86400 * 30), "/");
                setcookie("answer5",$answer5, time() + (86400 * 30), "/");
                header("location: page2.php");
            }else{
                echo "<div style='color: red;font-size: 20px;font-weight: bold;text-align: center;'>Hãy nhập trả lời tất cả các câu hỏi</div>";
            }

        }
        
    ?>
            <form name="registerForm" method="post" enctype="multipart/form-data" action="">
                <div class="main">
                    <div class="question_1">
                        <div class="question">Câu 1. Đoạn mã sau , in ra giá trị nào sau đây: $a=1; echo "a=$a"; </div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($listQA1 as $x_key){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$x_key." name='answer1'>"
                                            .$x_key.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                        <div class="cut"></div>
                    </div>
                    <div class="question_2">
                        <div class="question">Câu 2. Đoạn mã sau, in ra giá trị nào sau đây: echo 'HELLO WORLD!'; </div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($listQA2 as $x_key){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$x_key." name='answer2'>"
                                            .$x_key.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                        <div class="cut"></div>
                    </div>
                    <div class="question_3">
                        <div class="question">Câu 3. Đoạn mã nào sau đây được sử dụng để chú thích PHP</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($listQA3 as $x_key){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$x_key." name='answer3'>"
                                            .$x_key.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                        <div class="cut"></div>
                    </div>
                    <div class="question_4">
                        <div class="question">Câu 4. Mặc định của một biến không có giá trị được thể hiện với từ khóa</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($listQA4 as $x_key){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$x_key." name='answer4'>"
                                            .$x_key.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                        <div class="cut"></div>
                    </div>
                    <div class="question_5">
                        <div class="question">Câu 5. Ký hiệu nào được dùng khi sử dụng biến trong PHP</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($listQA5 as $x_key){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$x_key." name='answer5'>"
                                            .$x_key.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                        <div class="cut"></div>
                    </div>
                    <div class="btn-next">
                        <button >
                            NEXT
                        </button>
                    </div>  
                </div>
            </form>


</body>
</html>
