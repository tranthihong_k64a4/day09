<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        session_start();
    
        $listQA6 = array("const","constants","define","def");
        $listQA7 = array("1","0","0.5","1/2");
        $listQA8 = array("===","=","!=!","?=");
        $listQA9 = array("Preprocessed Hypertext Page", "Hypertext Transfer Protocol", "PHP: Hypertext Preprocessor", "Hypertext Markup Language");
        $listQA10 = array("ab","aa","bb","AB");

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            
            if(isset($_POST["answer6"])&&isset($_POST["answer7"])&&isset($_POST["answer8"])&&isset($_POST["answer9"])&&isset($_POST["answer10"])){
                $answer6 = $_POST["answer6"];
                $answer7 = $_POST["answer7"];
                $answer8 = $_POST["answer8"];
                $answer9 = $_POST["answer9"];
                $answer10 = $_POST["answer10"];
                setcookie("answer6",$answer6, time() + (86400 * 30), "/");
                setcookie("answer7",$answer7, time() + (86400 * 30), "/");
                setcookie("answer8",$answer8, time() + (86400 * 30), "/");
                setcookie("answer9",$answer9, time() + (86400 * 30), "/");
                setcookie("answer10",$answer10, time() + (86400 * 30), "/");
                header("location: page3.php");
            }else{
                echo "<div style='color: red;font-size: 20px;font-weight: bold;text-align: center;'>Hãy nhập trả lời tất cả các câu hỏi</div>";
            }

        }
        
    ?>
            <form name="registerForm" method="post" enctype="multipart/form-data" action="">
                <div class="main">
                    <div class="question_6">
                        <div class="question"> Câu 6: Hàm nào sau đây dùng để khai báo hằng số</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($listQA6 as $x){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$x." name='answer6'>"
                                            .$x.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                        <div class="cut"></div>
                    </div>
                    <div class="question_7">
                        <div class="question">Câu 7: Giá trị của tham số sau: $var = 1 / 2;</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($listQA7 as $x){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$x." name='answer7'>"
                                            .$x.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                        <div class="cut"></div>
                    </div>
                    <div class="question_8">
                        <div class="question">Câu 8: Đâu là phép toán được dùng so sánh trong PHP</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($listQA8 as $x){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$x." name='answer8'>"
                                            .$x.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                        <div class="cut"></div>
                    </div>
                    <div class="question_9">
                        <div class="question">Câu 9: PHP tượng trưng cho cái gì:</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($listQA9 as $x){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$x." name='answer9'>"
                                            .$x.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                        <div class="cut"></div>
                    </div>
                    <div class="question_10">
                        <div class="question">Câu 10: Đoạn mã sau, in ra giá trị nào sau đây: $var = 'a';$VAR = 'b';echo "$var$VAR";</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($listQA10 as $x){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value=".$x." name='answer10'>"
                                            .$x.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                        <div class="cut"></div>
                    </div>
                    <div class="btn-next">
                        <button >
                            Nộp Bài
                        </button>
                    </div>  
                </div>
            </form>


</body>
</html>
